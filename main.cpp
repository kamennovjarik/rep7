#include<string>                                                //commit 2
#include<vector>                                                //commit 4
using namespase std;                                            //commit  2

void main()                                                     //commit 1
{                                                               //commit 2
    string str = "Hello world!";                                //commit 3
    string str = "test str";                                    //commit 8
    cout << str << endl;                                        //commit 8
    std::vector<int> numbers{3, 4, 2, 9, 15, 267};              //commit 6
    int sum = sumVector(numbers);                               //commit 7
}                                                               //commit 2
                                                                
void multVector(vector<int> &numbers, int multNumber)           //commit 7
{                                                               //commit 7
    for (auto n : numbers) {                                    //commit 7
        n*= multNumber;                                         //commit 7
    }                                                           //commit 7
}                                                               //commit 7           

void printVector(vector<int> numbers)                           //commit 6
{                                                               //commit 4
    for (auto n : numbers) {                                    //commit 6
        std::cout << n << endl;                                 //commit 4
    }                                                           //commit 4
}                                                               //commit 4

int sumVector(vector<int> numbers)                              //commit 6
{                                                               //commit 6
    int res = 0;                                                //commit 6
    for (auto n : numbers) {                                    //commit 6
        res +=n;                                                //commit 6
    }                                                           //commit 6
    return res;                                                 //commit 6
}                                                               //commit 6


                                                                